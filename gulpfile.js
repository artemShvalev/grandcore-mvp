var gulp = require("gulp"),
  rename = require("gulp-rename"),
  sass = require("gulp-sass"),
  ts = require("gulp-typescript");

var paths = {
  cssSrc: "styles/*.scss",
  cssDist: "dist/styles",
  tsSrc: "scripts/*.ts",
  tsDist: "dist/scripts",
};

gulp.task("sass", function (done) {
  gulp
    .src("templates/**/*.scss")
    .pipe(sass())
    .pipe(rename({ dirname: "" }))
    .pipe(gulp.dest("static/"));

  done();
});

gulp.task("scripts", function (done) {
  gulp
    .src("templates/**/*.ts")
    .pipe(ts())
    .pipe(rename({ dirname: "" }))
    .pipe(gulp.dest("static/"));

  done();
});

gulp.task("watch", function () {
  gulp.watch("templates/**/*.scss", gulp.parallel("scripts"));
  gulp.watch("templates/**/*.scss", gulp.parallel("sass"));
});

//gulp.task("default", "sass");
gulp.task("default", gulp.series("scripts", "sass", "watch"));
